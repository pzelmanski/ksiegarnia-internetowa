﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KsiegarniaInternetowa
{
    public partial class MainWindow : Window
    {
        //
        // Nie ma lapania bledow
        // czasu mi na to zbrakło
        //


        List<List<Book>> GridData = new List<List<Book>>();
        List<ShoppingCart> ShoppingCartList = new List<ShoppingCart>();
        private bool[] _createdButtons = { false, false, false, false, false, false };
        int CartCounter = 0;
        public MainWindow()
        {
            InitializeComponent();

            txtSearch.MaxLength = 10;
            setLblCart();

            //troszke tragicznie wyszlo
            //robie selecta, zeby wywalic zbedne pola, np. ID z grida
            DbLibrary db = new DbLibrary();
            db.Database.CreateIfNotExists();

            GridData.Add(DbManager.GetQueryBooks(true, null, null, null, null));
            GridData.Add(DbManager.GetQueryBooks(null, null, null, true, null));
            GridData.Add(DbManager.GetQueryBooks(null, 1, null, null, null));
            GridData.Add(DbManager.GetQueryBooks(null, 2, null, null, null));
            GridData.Add(DbManager.GetQueryBooks(null, null, true, null, null));
            GridData.Add(DbManager.GetQueryBooks(null, null, null, null, true));

            dgAll.ItemsSource = GridData[0].Select(x => new { x.Name, x.Releasedate, x.Price, x.Author, x.Publisher }).ToList();
            dgAnnoucement.ItemsSource = GridData[1].Select(x => new { x.Name, x.Releasedate, x.Price, x.Author, x.Publisher }).ToList();
            dgAudioBooks.ItemsSource = GridData[2].Select(x => new { x.Name, x.Releasedate, x.Price, x.Author, x.Publisher }).ToList();
            dgEbooks.ItemsSource = GridData[3].Select(x => new { x.Name, x.Releasedate, x.Price, x.Author, x.Publisher }).ToList();
            dgNew.ItemsSource = GridData[4].Select(x => new { x.Name, x.Releasedate, x.Price, x.Author, x.Publisher }).ToList();
            dgSuperOppurtunity.ItemsSource = GridData[5].Select(x => new { x.Name, x.Releasedate, x.Price, x.Author, x.Publisher }).ToList();


        }


        //Change headers in dataGrids
        private void SetGridHeaders()
        {
            DataGrid[] grids = { dgAll, dgAnnoucement, dgAudioBooks, dgEbooks, dgNew, dgSuperOppurtunity };
            foreach (var dg in grids)
            {
                if (dg.Columns.Count == 5)
                {
                    dg.Columns[0].Header = "Tytuł";
                    dg.Columns[1].Header = "Data wydania";
                    dg.Columns[2].Header = "Cena";
                    dg.Columns[3].Header = "Autor";
                    dg.Columns[4].Header = "Wydawca";
                }
            }
        }
        private void CreateButtons()
        {
            if (!_createdButtons[tabControl.SelectedIndex])
            {
                var col = new DataGridTemplateColumn { Header = "" };

                var dt = new DataTemplate();

                var stkpnl = new FrameworkElementFactory(typeof(StackPanel));
                stkpnl.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
                var btn1 = new FrameworkElementFactory(typeof(Button));

                btn1.SetValue(ContentProperty, "Do koszyka");
                btn1.AddHandler(ButtonBase.ClickEvent, new RoutedEventHandler(addToCart));
                btn1.SetValue(MarginProperty, new Thickness(0, 0, 10, 0));
                stkpnl.AppendChild(btn1);

                dt.VisualTree = stkpnl;
                col.CellTemplate = dt;
                //powtarzam sie, juz trudno
                DataGrid[] grids = { dgAll, dgAnnoucement, dgAudioBooks, dgEbooks, dgNew, dgSuperOppurtunity };

                grids[tabControl.SelectedIndex].Columns.Add(col);
                _createdButtons[tabControl.SelectedIndex] = true;
            }
            
        }
        private void addToCart(object sender, RoutedEventArgs e)
        {
            dgAll.SelectedCells.ToString();
            var selectedIndex = dgAll.SelectedIndex;
            //MessageBox.Show(selectedIndex.ToString());
            AddToCartWindow w = new AddToCartWindow();
            if(w.ShowDialog() == false)
            {
                CartCounter += w.Qty;
                setLblCart();
            }
        }

        private void setLblCart()
        {
            lblCart.Content = "Zawartość koszyka: " + CartCounter + " produkt(-y)(-ów)";
        }

        private void AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyType == typeof(System.DateTime))
                (e.Column as DataGridTextColumn).Binding.StringFormat = "dd/MM/yyyy";
        }

        private void GridLoaded(object sender, RoutedEventArgs e)
        {
            SetGridHeaders();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            DataGrid[] grids = { dgAll, dgAnnoucement, dgAudioBooks, dgEbooks, dgNew, dgSuperOppurtunity };

            var searchedData =
               GridData[tabControl.SelectedIndex].
               Select(x => new { x.Name, x.Releasedate, x.Price, x.Author, x.Publisher }).
               Where(x => x.Name.ToUpper().Contains(txtSearch.Text.ToUpper()) ||
               x.Author.ToUpper().Contains(txtSearch.Text.ToUpper())).
               ToList();

            if (searchedData.Capacity > 0)
                grids[tabControl.SelectedIndex].ItemsSource = searchedData;
            else
                MessageBox.Show("Brak rekordów");
        }
        
        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CreateButtons();
        }
    }
}
