namespace KsiegarniaInternetowa.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<KsiegarniaInternetowa.DbLibrary>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(KsiegarniaInternetowa.DbLibrary context)
        {
            //  This method will be called after migrating to the latest version.

            //===============================================================
            //                  INFO DLA SPRAWDZAJACEGO                    ||
            //wiem, moglem to zrobic kluczami obcymi - wydawnictwo, autora.||
            //      Wczesniej na to nie wpadlem, juz zostawie jak jest.    ||
            //===============================================================
            context.Books.AddOrUpdate(new Book(1, 1, "Tytul 1", new DateTime(2015, 01, 01), 19.22, "Autor 1", "Wydawnictwo 1", false));
            context.Books.AddOrUpdate(new Book(2, 1, "Tytul 2", new DateTime(2014, 02, 01), 19.23, "Autor 1", "Wydawnictwo 2", true));
            context.Books.AddOrUpdate(new Book(3, 1, "Tytul 3", new DateTime(2013, 03, 01), 19.24, "Autor 2", "Wydawnictwo 3", false));
            context.Books.AddOrUpdate(new Book(4, 2, "Tytul 4", new DateTime(2012, 04, 01), 19.25, "Autor 2", "Wydawnictwo 1", false));
            context.Books.AddOrUpdate(new Book(5, 2, "Tytul 5", new DateTime(2011, 05, 01), 19.26, "Autor 3", "Wydawnictwo 2", false));
            context.Books.AddOrUpdate(new Book(6, 2, "Tytul 6", new DateTime(2010, 06, 01), 19.27, "Autor 3", "Wydawnictwo 3", true));
            context.Books.AddOrUpdate(new Book(7, 2, "Tytul 7", new DateTime(2016, 10, 29), 19.15, "Autor 4", "Wydawnictwo 4", true));
            context.Books.AddOrUpdate(new Book(8, 2, "Tytul 8", new DateTime(2016, 11, 08), 19.17, "Autor 4", "Wydawnictwo 4", false));
            context.Books.AddOrUpdate(new Book(9, 2, "AATytul 9", new DateTime(2016, 11, 09), 19.37, "Autor 5", "Wydawnictwo 5", true));

            context.ShoppingCartSettings.AddOrUpdate(new ShoppingCartSetting(1, "Okladka"));
            context.ShoppingCartSettings.AddOrUpdate(new ShoppingCartSetting(2, "Wydawnictwo"));
            context.ShoppingCartSettings.AddOrUpdate(new ShoppingCartSetting(3, "Wydanie"));
            context.ShoppingCartSettings.AddOrUpdate(new ShoppingCartSetting(4, "Nosnik"));

            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(1, 1, "Twarda"));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(2, 1, "Miekka"));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(3, 2, "Wydawnictwo 1"));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(4, 2, "Wydawnictwo 2"));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(5, 2, "Wydawnictwo 3"));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(6, 3, "Wydanie 1", 1994));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(7, 3, "Wydanie 2", 1998));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(8, 3, "Wydanie 3", 2002));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(9, 3, "Wydanie 4", 2006));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(10, 4, "CD"));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(11, 4, "DVD"));
            context.ShoppingCartSettingsPositions.AddOrUpdate(new ShoppingCartSettingPosition(12, 4, "Pendrive"));
            context.SaveChanges();
        }
    }
}
