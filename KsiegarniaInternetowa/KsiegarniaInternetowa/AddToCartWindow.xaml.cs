﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KsiegarniaInternetowa
{
    /// <summary>
    /// Interaction logic for AddToCartWindow.xaml
    /// </summary>
    public partial class AddToCartWindow : Window
    {
        public int Qty { get { return Int32.Parse(txtQty.Text); } }
        List<List<ShoppingCartSettingPosition>> settingsPositions = new List<List<ShoppingCartSettingPosition>>();
        public AddToCartWindow()
        {
            InitializeComponent();
            List<ShoppingCartSetting> settingsList = DbManager.GetCartSettings();
            foreach (var s in settingsList)
            {
                settingsPositions.Add(DbManager.GetCartSettingsPositions(s.SettingId));
            }
            cbCover.ItemsSource = settingsPositions[0].Select(x => x.PositionName);
            cbPublisher.ItemsSource = settingsPositions[1].Select(x => x.PositionName);
            cbEdition.ItemsSource = settingsPositions[2].Select(x => x.PositionName);
            cbDriver.ItemsSource = settingsPositions[3].Select(x => x.PositionName);
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            int qty;
            if (txtQty.Text == "")
            {
                MessageBox.Show("pusta ilość");
                return;
            }
            if (Int32.TryParse(txtQty.Text, out qty))
                if (qty < 0 || qty > 999)
                {
                    MessageBox.Show("Ilość musi być 0-999");
                    return;
                }
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            txtQty.Text = "0";
            this.Close();
        }

        private void cbEdition_DropDownClosed(object sender, EventArgs e)
        {
            lblYear.Content = settingsPositions[2].
                Select(x => new { x.Year, x.PositionName }).
                Where(x => x.PositionName == cbEdition.Text).ToList()[0].Year;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtQty.Text == "")
                txtQty.Text = "0";
        }
    }
}
