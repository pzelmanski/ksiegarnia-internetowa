﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KsiegarniaInternetowa
{
    public static class DbManager
    {
        /// <summary>
        /// Gets data to fill DataSets
        /// </summary>
        /// <param name="iType">1 - audio, 2 - ebook</param>
        /// <param name="iNewBooks">(today - 14) : (today)</param>
        /// <param name="iIncomingBooks">(today) : (today + 14)</param>
        /// <param name="iSuperOppurtunity">true - only get super oppurtunities</param>
        /// <returns></returns>
        public static List<Book> GetQueryBooks(bool? iAll, int? iType, bool? iNewBooks, bool? iIncomingBooks, bool? iSuperOppurtunity)
        {
            DbLibrary db = new DbLibrary();
            List<Book> returnedValue = new List<Book>();
            var query = db.Books;
            if (iAll == true)
                returnedValue = query.ToList();
            if (iType != null)
                returnedValue = query.Select(x => x).Where(b => b.BookType == iType).ToList();
            if (iNewBooks == true)
            {
                DateTime tempDate = DateTime.Today.AddDays(-14);
                returnedValue = query.Select(x => x).Where(b => b.Releasedate >= tempDate && b.Releasedate <= DateTime.Today).ToList();
            }
            if (iIncomingBooks == true)
            {
                DateTime tempDate = DateTime.Today.AddDays(14);
                returnedValue = query.Select(x => x).Where(b => b.Releasedate > DateTime.Today && b.Releasedate <= tempDate).ToList();
            }
            if (iSuperOppurtunity == true)
                returnedValue = query.Select(x => x).Where(b => b.SuperOppurtunity == true).ToList();


            return returnedValue;
        }

        public static List<ShoppingCartSetting> GetCartSettings()
        {
            DbLibrary db = new DbLibrary();
            var query = db.ShoppingCartSettings;

            return query.ToList();
        }

        public static List<ShoppingCartSettingPosition> GetCartSettingsPositions(int iSettingId)
        {
            DbLibrary db = new DbLibrary();
            var query = db.ShoppingCartSettingsPositions;
            List<ShoppingCartSettingPosition> returnedValue =
                query.Select(x => x).Where(x => x.ShoppingCartSettingId == iSettingId).ToList();

            return returnedValue;
        }
    }
}
