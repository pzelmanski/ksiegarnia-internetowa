namespace KsiegarniaInternetowa
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    //nullability, column size
    using System.ComponentModel.DataAnnotations;
    //more database data annotations
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Collections;
    using System.Collections.Generic;

    public class DbLibrary : DbContext
    {

        // Your context has been configured to use a 'DbLibrary' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'KsiegarniaInternetowa.DbLibrary' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'DbLibrary' 
        // connection string in the application configuration file.
        public DbLibrary()
            : base("name=DbLibrary")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public DbSet<Book> Books { get; set; }
        public DbSet<ShoppingCartSetting> ShoppingCartSettings { get; set; }
        public DbSet<ShoppingCartSettingPosition> ShoppingCartSettingsPositions { get; set; }
    }

    public class Book
    {
        public Book() { }
        //===============================================================
        //                  INFO DLA SPRAWDZAJACEGO                    ||
        //wiem, moglem to zrobic kluczami obcymi - wydawnictwo, autora.||
        //      Wczesniej na to nie wpadlem, juz zostawie jak jest.    ||
        //===============================================================

        public Book(int iId, int iBookType, string iName, DateTime iReleaseDate, double iPrice, string iAuthor, string iPublisher, bool iSuperOppurtunity)
        {
            BookId = iId;
            BookType = iBookType; 
            Name = iName;
            Releasedate = iReleaseDate.Date;
            Price = Math.Round(iPrice, 2);
            Author = iAuthor;
            Publisher = iPublisher;
            SuperOppurtunity = iSuperOppurtunity;
        }
        public Book(string iName, int iBookType, DateTime iReleaseDate, double iPrice, string iAuthor, string iPublisher, bool iSuperOppurtunity)
        {
            BookType = iBookType;
            Name = iName;
            Releasedate = iReleaseDate.Date;
            Price = Math.Round(iPrice, 2);
            Author = iAuthor;
            Publisher = iPublisher;
            SuperOppurtunity = iSuperOppurtunity;
        }
        public void SetSuperOppurtunity(bool iIsSuper)
        {
            SuperOppurtunity = iIsSuper;
        }
        //[NotMapped]
        [Key, Required]
        public int BookId { get; set; }
        [Required, Range(1, 2)]
        public int BookType { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Releasedate { get; set; }
        [Required]
        public double Price { get; set; }
        [MaxLength(15), Required]
        public string Author { get; set; }
        [Required]
        public string Publisher { get; set; }
        public bool SuperOppurtunity { get; set; }
    }

    public class ShoppingCart
    {
        public ShoppingCart() { }
        public ShoppingCart(string iCover, string iPublisher, string iEdition, int iYear, int iQty)
        {
            Cover = iCover;
            Publisher = iPublisher;
            Edition = iEdition;
            Year = iYear;
            Qty = iQty;
        }
        public string Cover;
        public string Publisher;
        public string Edition;
        public int Year;
        public int Qty;
    }
    public class ShoppingCartSetting
    {
        public ShoppingCartSetting() { }
        public ShoppingCartSetting(int iId, string iName)
        {
            SettingId = iId;
            Name = iName;
        }
        //Main ID, Identyfying setting's id
        [Key]
        public int SettingId { get; set; }
        [Required]
        public string Name { get; set; }
        //public ICollection<ShoppingCartSettingPosition> SettingPositions { get; set; }
    }
    /// <summary>
    /// field's option ID - hard cover, soft cover..
    /// </summary>
    public class ShoppingCartSettingPosition
    {
        public ShoppingCartSettingPosition() { }
        public ShoppingCartSettingPosition(int iPositionId, int iSettingId, string iName)
        {
            SettingPositionId = iPositionId;
            ShoppingCartSettingId = iSettingId;
            PositionName = iName;
            Year = null;
        }
        public ShoppingCartSettingPosition(int iPositionId, int iSettingId, string iName, int iYear)
        {
            SettingPositionId = iPositionId;
            ShoppingCartSettingId = iSettingId;
            PositionName = iName;
            Year = iYear;
        }

        [Key]
        public int SettingPositionId { get; set; }
        [Required]
        public string PositionName { get; set; }
        public int? Year { get; set; }
        [ForeignKey("Setting")]
        [Required]
        public int ShoppingCartSettingId { get; set; }
        public ShoppingCartSetting Setting { get; set; }
    }
}
